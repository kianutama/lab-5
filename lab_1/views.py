from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Kianutama Radianur' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
curr_month = int(datetime.now().strftime("%m"))
curr_date = int(datetime.now().strftime("%d"))
birth_date = date(1998,10,18) #TODO Implement this, format (Year, Month, Date)
bday = ''
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
